import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';

const BinarNews = () => {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}

export default BinarNews;
