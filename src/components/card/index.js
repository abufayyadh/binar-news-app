import React from 'react';
import {View, Text, Image, StyleSheet, Dimensions} from 'react-native';
import LinkButton from '../button';
import {WebView} from 'react-native-webview';

const {width, height} = Dimensions.get('window');

const NewsCard = ({item}) => {
  return (
    <View style={styles.card}>
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.date}>{item.publishedAt}</Text>
      <Text style={styles.author}>Author: {item.author}</Text>
      <Image
        style={styles.image}
        source={item.urlToImage ? {uri: item.urlToImage} : null}
      />
      <Text style={styles.description}>{item.description}</Text>
      <View style={styles.button}>
        <LinkButton title="Read article" url={item.url} />
      </View>
    </View>
  );
};

export default NewsCard;

const styles = StyleSheet.create({
  card: {
    backgroundColor: 'white',
    margin: width * 0.03,
    borderRadius: width * 0.05,
    borderColor: 'black',
    shadowColor: 'black',
    shadowOffset: {width: 0.5, height: 0.5},
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  title: {
    marginVertical: width * 0.03,
    marginHorizontal: width * 0.05,
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    marginBottom: width * 0.01,
    marginHorizontal: width * 0.05,
    color: 'gray',
    fontSize: 18,
  },
  image: {
    height: height / 6,
    marginLeft: width * 0.05,
    marginRight: width * 0.05,
    marginVertical: height * 0.02,
    borderRadius: width * 0.05,
  },
  date: {
    marginBottom: width * 0.0,
    marginHorizontal: width * 0.05,
    fontSize: 12,
    color: 'black',
  },
  author: {
    marginBottom: width * 0.0,
    marginHorizontal: width * 0.05,
    fontSize: 15,
    color: 'gray',
  },
  button: {
    marginBottom: 5,
  },
});
