import React from 'react';
import {Text, StyleSheet, TouchableOpacity, Linking} from 'react-native';

const Button = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

// export default Button;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#9b59b6',
    paddingVertical: 10,
    borderRadius: 20,
    marginLeft: 20,
    marginRight: 220,
  },
  text: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
  },
});

const LinkButton = (props) => {
  return (
    <Button
      title={props.title}
      onPress={() => {
        Linking.openURL(props.url).catch((err) => {
          console.error('Failed opening page because: ', err);
          alert('Failed to open page');
        });
      }}
    />
  );
};

export default LinkButton;
