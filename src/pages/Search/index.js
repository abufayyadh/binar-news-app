import React, {useState, useEffect, Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Dimensions,
  TextInput,
} from 'react-native';
import NewsCard from '../../components/card';
import NewsAPI from '../../apis';
import {SearchBar} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

export default class Search extends Component {
  state = {
    search: '',
  };

  updateSearch = (search) => {
    this.setState({search});
  };

  render() {
    const {search} = this.state;

    return (
      <SearchBar
        placeholder="Type Here..."
        onChangeText={this.updateSearch}
        value={search}
      />
    );
  }
}
