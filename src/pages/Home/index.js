import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, StyleSheet, Dimensions} from 'react-native';
import NewsCard from '../../components/card';
import NewsAPI from '../../apis';
import Config from 'react-native-config';
const {width, height} = Dimensions.get('window');

const Home = () => {
  const [news, setNews] = useState([]);

  useEffect(() => {
    getNewsAPI();
  }, []);

  function getNewsAPI() {
    NewsAPI.get(`top-headlines?country=id&apiKey=${Config.API_KEY}&page=1`)
      .then(async function (response) {
        setNews(response.data);
        console.log(news);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  if (!news) {
    return null;
  }

  return (
    <View>
      <View style={styles.card}>
        <Text style={styles.title}>WELCOME TO BINAR NEWS</Text>
      </View>

      <FlatList
        data={news.articles}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={({item}) => {
          // return <NewsCard />;
          return <NewsCard item={item} />;
        }}
      />

      {/* <View>
        <Text
          style={{
            color: 'black',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 20,
          }}>
          WELCOME TO BINAR NEWS
        </Text>
      </View>
      <View>
        <NewsCard></NewsCard> */}
      {/* </View> */}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#8e44ad',
    margin: width * 0.03,
    borderRadius: width * 0.05,
    borderColor: 'black',
    fontSize: 20,
    fontWeight: 'bold',
  },
  title: {
    marginVertical: width * 0.03,
    marginHorizontal: width * 0.05,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
