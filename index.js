/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
// import App from './src/AppTest';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
